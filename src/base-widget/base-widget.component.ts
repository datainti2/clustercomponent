import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

declare var CarrotSearchCircles: any;
import '../../assets/includes/circles/carrotsearch.circles.asserts.js';
import '../../assets/includes/circles/carrotsearch.circles.js';

@Component({
  selector: 'app-base-widget',
  template: '<div id="visualization"></div>',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgeClusterComponent implements OnInit  {
  title = 'app works!';
  public allItems: any[];
  @Output() onSelectIssue: EventEmitter<any> = new EventEmitter();
  @Input() autoselect: boolean = true;

  constructor(){ }

  ngOnInit() {

  this.allItems = [
                {
          "data": [{
            "total": 147,
            "issue": "Justice Must Not Only Be Done But Must Be Sen To Be Done",
            "ids": [
              "1310314472363320_1311417238919710",
              "1310314472363320_1311388208922613",
              "1310314472363320_1311175405610560",
              "1310314472363320_1310324109029023",
              "1310314472363320_1310320985696002",
              "1310314472363320_1310320765696024"
            ]
          }, {
            "total": 3,
            "issue": "Sb Ya Rakyat Diberik Keistimewan Dpt Ketua Menteri Yg Bagus Sik Lamak",
            "ids": [
              "1310314472363320_1310583095669791",
              "1304576222937145_1305863062808461",
              "1304576222937145_1304975522897215",
              "1310314472363320_1310989592295808",
              "1310314472363320_1310917495636351",
              "1310314472363320_1310788372315930",
              "1310314472363320_1310750955653005",
              "1310314472363320_1310750888986345",
              "1310314472363320_1310371952357572",
              "1310314472363320_1310367772357990",
              "1310314472363320_1310361725691928",
              "1310314472363320_1310360255692075",
              "1310314472363320_1310356602359107",
              "1310314472363320_1310354265692674",
              "1310314472363320_1310353379026096",
              "1310314472363320_1310344115693689",
              "1310314472363320_1310342912360476",
              "1310314472363320_1310339859027448",
              "1310314472363320_1310335579027876",
              "1310314472363320_1310334895694611",
            ]
          }, {
            "total": 2,
            "issue": "Tok Nan Dh Lamak Mampus BN Kau Tok",
            "ids": [
              "1310314472363320_1310324802362287",
              "1310314472363320_1310324572362310"
            ]
          }, {
            "total": 2,
            "issue": "Najib Memerintah Slgik Ya Lh Rkyt Trutama Yg Brpendapatan Rndh Trtekan",
            "ids": [
              "1310314472363320_1310368412357926",
              "1304576222937145_1306250466103054"
            ]
          }, {
            "total": 1,
            "issue": "",
            "ids": [
              "1296150470446387_1296175180443916"
            ]
          }, {
            "total": 1,
            "issue": "Anang Ngundi Bangsa Laut",
            "ids": [
              "1304576222937145_1306553276072773"
            ]
          }, {
            "total": 1,
            "issue": "Bang",
            "ids": [
              "1299389873455780_1300438566684244"
            ]
          }, {
            "total": 1,
            "issue": "Molah Cerita",
            "ids": [
              "1310314472363320_1310560015672099"
            ]
          }, {
            "total": 1,
            "issue": "Bedau Bc Ai Paip Kelia Ngai Bala Perintah Meri Tangki",
            "ids": [
              "1310314472363320_1310640315664069"
            ]
          }, {
            "total": 1,
            "issue": "Ne Kali La Jgn",
            "ids": [
              "1310314472363320_1311220618939372"
            ]
          }, {
            "total": 1,
            "issue": "Because You Want To Satisfy Some Rainforest Raper Do Not Give Them Anymore",
            "ids": [
              "1304576222937145_1304632192931548"
            ]
          }, {
            "total": 1,
            "issue": "Legasi Arwah Tok Nan",
            "ids": [
              "1304576222937145_1304610332933734"
            ]
          }, {
            "total": 1,
            "issue": "Is BN X Suka UMNO Sokong BN Ape Bezanya",
            "ids": [
              "1310314472363320_1310333945694706"
            ]
          }, {
            "total": 1,
            "issue": "bena2 Sarawak Towk Abg Jo Oi",
            "ids": [
              "1304576222937145_1305347089526725"
            ]
          }, {
            "total": 1,
            "issue": "Jobs",
            "ids": [
              "1241500405911394_1241871939207574"
            ]
          }, {
            "total": 1,
            "issue": "To Us You Realy Care About Rakyat",
            "ids": [
              "1299389873455780_1299511683443599"
            ]
          }, {
            "total": 1,
            "issue": "Bagus X Payah Turun M",
            "ids": [
              "1310314472363320_1310515019009932"
            ]
          }, {
            "total": 1,
            "issue": "Abg Jo Tok Nang Sik Prihatin Dgn",
            "ids": [
              "1304576222937145_1304916042903163"
            ]
          }, {
            "total": 1,
            "issue": "Dibawah Kerajan Yg Dorg Bangakan Tu",
            "ids": [
              "1310314472363320_1310365889024845"
            ]
          }],
          "status": true,
          "message": "success"
        }
      ];

      var dt = this.allItems[0];
      var dt1 = dt.data;
      const groups = [dt];
      let data_selected = true;
      for (let i = 0; i < dt1.length; i++) {
        if (i > 0) data_selected = false;
        groups.push(
          {
            data_ids: dt1[i]['ids'],
            label: dt1[i]['issue'],
            selected: data_selected, weight: dt1[i]['total']
          });
      }
      this.loadCluster(groups);
      console.log(this.loadCluster);
  }
  loadCluster(groups) {
    const self = this;
    const circles = new CarrotSearchCircles({
      id: 'visualization',
      pixelRatio: Math.min(1.5, window.devicePixelRatio || 1),
      dataObject: {
        groups: groups
      },
      captureMouseEvents: false,
      rainbowStartColor: 'rgb(255,52,6)',
      rainbowEndColor: 'rgb(76,255,145)',
      groupColorDecorator: function (opts, params, vars) {
        vars.labelColor = 'auto';
      },
      groupOutlineColor: '#fff',
      diameter: function (box) {
        return Math.min(box.height - 1 * 40, box.width);
      },
      centery: '50%',
      titleBar: 'inscribed',
      titleBarTextColor: 'rgba(0,0,0,.3)',
      titleBarFontFamily: 'calibri',
      groupFontFamily: 'calibri',
      titleBarMaxFontSize: 30,
      ringScaling: 1.5,
      ratioAspectSwap: 10000,
      onGroupHover: function (attrs) {
        '';
      },
      onGroupClick: function (attrs) {
        self.onSelectIssue.emit(attrs.group);
      },
      onRolloutComplete: function () {
        if (self.autoselect) {
          self.onSelectIssue.emit(circles.get('selection').groups[0]);
        }
      },
      visibleGroupCount: 10,
      onGroupOpenOrClose: function (expand) {

      }
    });
  }



}
