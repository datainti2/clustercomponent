import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { BaseWidgeClusterComponent } from './src/base-widget/base-widget.component';

export * from './src/base-widget/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
	MaterialModule
  ],
  declarations: [
	BaseWidgeClusterComponent
  ],
  exports: [
  BaseWidgeClusterComponent
  ]
})
export class ClusterModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ClusterModule,
      providers: []
    };
  }
}
